import { test as base, expect } from '@playwright/test';
import { MainPage, mainPageFixture} from '../../../pages/main/__page-object__';
import { RatingPage, ratingPageFixture} from '../__page-object__'

const test = base.extend<{ mainPage: MainPage, ratingPage: RatingPage }>({
  mainPage: mainPageFixture,
  ratingPage: ratingPageFixture,
});


test.skip('При ошибке сервера в методе raiting - отображается попап ошибки. Через route.fulfill() + Page Object', async ({ page, mainPage, ratingPage }) => {
  // Замокали  
  await ratingPage.mockRequest();

  // Перешли на стартовую страницу  
  await mainPage.openMainPage();

  // Перешли на страницу с рейтингом котов  
  await mainPage.goToRatingPage();

  // Сравнили полученный текст с ожидаемым  
  await expect(await page.locator(ratingPage.toast).textContent(), 'Сообщение некорректно').toEqual('Ошибка загрузки рейтинга');  
});

test.skip('Рейтинг котиков отображается + Page Object', async ({ page, mainPage, ratingPage }) => {
  
    // Перешли на стартовую страницу  
  await mainPage.openMainPage();

  // Перешли на страницу с рейтингом котов  
  await mainPage.goToRatingPage();

  // Проверили что отображено 10 элементов 
  await expect(page.locator(ratingPage.cellSelector)).toHaveCount(10);

  // Собрали значения лайков 
  let likes = await page.locator(ratingPage.cellSelector).allTextContents();

  // Отобразили список найденнных значений
  console.log(likes);
  
  // Проверили что лайки отсортированы по убыванию
  expect.soft(ratingPage.isArraySortedDesc(likes), 'Список лайков отсортирован не по убыванию').toEqual(true);
});