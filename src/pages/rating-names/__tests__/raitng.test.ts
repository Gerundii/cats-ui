import { expect, test } from '@playwright/test';

test('При ошибке сервера в методе raiting - отображается попап ошибки. Через route.abort()', async ({ page   }) => {
  
  // Прервали запрос  
  await page.route('*/**/api/likes/cats/rating', route => route.abort());

  // Перешли на стартовую страницу  
  await page.goto('/');

  // Перешли на страницу с рейтингом котов  
  await page.getByRole('link', { name: 'Рейтинг имён' }).click();

  // Сохранили текст сообщения
  let toastText = await page.locator('//div[contains(@class,"ajs-visible")]').textContent();

  // Сравнили полученный текст с ожидаемым  
  expect(toastText, 'Сообщение некорректно').toEqual('Ошибка загрузки рейтинга');
});

test('При ошибке сервера в методе raiting - отображается попап ошибки. Через route.fulfill()', async ({ page   }) => {
  
  // Замокали  
  await page.route(
    request => request.href.includes('/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
        body: 'Internal Server Error'
      });
    }
  );

  // Перешли на стартовую страницу  
  await page.goto('/');

  // Перешли на страницу с рейтингом котов  
  await page.getByRole('link', { name: 'Рейтинг имён' }).click();

  // Сохранили текст сообщения
  let toastText = await page.locator('//div[contains(@class,"ajs-visible")]').textContent();

  // Сравнили полученный текст с ожидаемым  
  expect(toastText, 'Сообщение некорректно').toEqual('Ошибка загрузки рейтинга');
});

test('Рейтинг котиков отображается', async ({ page   }) => {
  
  // Функция для анализа расположения рейтинга котов по убыванию
  function isArraySortedDesc(array) {
    for (let i = 0; i < array.length - 1; i++) {
      if (+array[i] < +array[i + 1]) {
        return false;
      }
    }
    return true;
  }  

  // Перешли на стартовую страницу
  await page.goto('/');

  // Перешли на страницу с рейтингом котов
  await page.getByRole('link', { name: 'Рейтинг имён' }).click();

  // Проверили что отображено 10 элементов 
  await expect(page.locator('//td[contains(@class,"has-text-success")]')).toHaveCount(10);

  // Собрали значения лайков 
  let likes = await page.locator('//td[contains(@class,"has-text-success")]').allTextContents();

  // Отобразили список найденнных значений
  console.log(likes);
  
  // Проверили что лайки отсортированы по убыванию
  expect.soft(isArraySortedDesc(likes), 'Список лайков отсортирован не по убыванию').toEqual(true);
});