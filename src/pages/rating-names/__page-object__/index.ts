import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики со страницей рейтинга котиков.
 *
 */
export class RatingPage {
  private page: Page;
  public toast: string = '//div[contains(@class,"ajs-visible")]';
  public cellSelector: string = '//td[contains(@class,"has-text-success")]';
  

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page
  }

  async mockRequest() {
    return await test.step('Мокаем запрос 500 ошибкой', async () => {
      await this.page.route(
        request => request.href.includes('/likes/cats/rating'),
        async route => {
          await route.fulfill({
            status: 500,
            body: 'Internal Server Error'
          });
        }
      );
    })
  }

  isArraySortedDesc(array) {
    for (let i = 0; i < array.length - 1; i++) {
      if (+array[i] < +array[i + 1]) {
        return false;
      }
    }
    return true;
  }  
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};