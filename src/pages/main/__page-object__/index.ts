import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики с главной страницей приложения котиков.
 *
 */
export class MainPage {
  private page: Page;
  public input: string = '//input[@placeholder]'
  public buttonSelector: string = '//button[@type="submit"]';
  public buttonRating: string = '//a[contains(@href,"/rating")]'

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page
  }


  async openMainPage() {
    return await test.step('Открываю главную страницу приложения', async () => {
      await this.page.goto('/')
    })
  }

  async inputInSearch(data: string) {
    return await test.step(`Ввожу в строку поиска данные ${data}`, async () => {
      await this.page.fill(this.input, data);
    })
  }

  async goToRatingPage() {
    return await test.step(`Перехожу на страницу рейтинга`, async () => {
      await this.page.click(this.buttonRating);
    })
  }
}

export type MainPageFixture = TestFixture<
  MainPage,
  {
    page: Page;
  }
  >;

export const mainPageFixture: MainPageFixture = async (
  { page },
  use
) => {
  const mainPage = new MainPage({ page });

  await use(mainPage);
};

